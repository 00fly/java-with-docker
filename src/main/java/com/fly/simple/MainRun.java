package com.fly.simple;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.mysql.cj.jdbc.MysqlDataSource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MainRun
{
    /**
     * 线程池保证程序一直运行
     * 
     * @param args
     */
    public static void main(String[] args)
    {
        // 输出System env
        log.info("------------------------------");
        System.getenv()
            .keySet()
            .stream()
            .sorted() // 排序
            .forEach(k -> log.info("{} -> {}", k, System.getenv().get(k)));
        log.info("------------------------------");
        
        // 打印DataSource
        MysqlDataSource dataSource = BaseDataSource.getDataSource();
        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(() -> {
            log.info("{}", dataSource.getUrl());
            log.info("{}", dataSource.getUser());
            log.info("{}", dataSource.getPassword());
        }, 0, 600, TimeUnit.SECONDS);
    }
}
